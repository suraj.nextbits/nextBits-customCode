Call CMS blocks on frontend using admin CMS page

    Admin => Content => Blocks => Add New Block
        Block Title -> Admin CMS Block
        Identifier -> admin-cms-block
        Content -> Custom Admin Cms Block
    Admin => Content => Pages => Edit Home Page
        Add line in content
    {{block class="Magento\Cms\Block\Block" name="custom-admin-cms-block" block_id="admin-cms-block"}}

Call CMS blocks on frontend using code in XML or PHTML
    
    Create a cms_index_index.xml file and call admin cms block as block-identifier : admin-cms-block

Create widgets and arguments (Featured products from specific category)
    
    Widget Created and passed products_count & id_path as argument
    id_path - refers to category id
    products_count - set page size


