<?php

namespace NextBits\CustomCode\Block\Widget;

use Magento\Catalog\Block\Product\Image;
use Magento\Catalog\Block\Product\ImageBuilder;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use  Magento\Widget\Block\BlockInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use RuntimeException;

class FeaturedProducts extends Template implements BlockInterface
{
    protected $_template = "NextBits_CustomCode::widget/products/featured.phtml";

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var Visibility
     */
    private $catalogProductVisibility;

    /**
     * Default value for products count that will be shown
     */
    const DEFAULT_PRODUCTS_COUNT = 10;

    /**
     * @var ImageBuilder
     */
    private $imageBuilder;

    /**
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param Visibility $catalogProductVisibility
     * @param ImageBuilder $imageBuilder
     * @param array $data
     */
    public function __construct(
        Template\Context  $context,
        CollectionFactory $collectionFactory,
        Visibility $catalogProductVisibility,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        array             $data = []
    ) {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->imageBuilder = $imageBuilder;
    }

    public function getPageSize(){
        if ($this->hasData('products_count')){
            return $this->getData('products_count');
        }
        return self::DEFAULT_PRODUCTS_COUNT;
    }

    /**
     * @throws RuntimeException
     * @return int
     */
    protected function getCategoryId()
    {
        if ($this->hasData('id_path')){
            $idPath =  $this->getData('id_path');
            $rewriteData = explode('/', $idPath);
            if (!isset($rewriteData[0]) || !isset($rewriteData[1])) {
                throw new RuntimeException('Wrong id_path structure.');
            }
            return $rewriteData[1];
        }
        return 2;
    }

    /**
     * @return Collection
     */
    public function getProductCollection(): Collection
    {
        return $this->collectionFactory->create()
            ->addAttributeToSelect(['name','is_featured','status','visibility','thumbnail'])
            ->addAttributeToFilter('is_featured',['eq' => 1])
            ->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds())
            ->addCategoriesFilter(['eq' => $this->getCategoryId()])
        ->setPageSize($this->getPageSize())
        ->setCurPage( 1);
    }

    /**
     * Retrieve product image
     *
     * @param Product $product
     * @param string $imageId
     * @param array $attributes
     * @return Image
     */
    public function getImage(Product $product, string $imageId, array $attributes = [])
    {
        return $this->imageBuilder->create($product, $imageId, $attributes);
    }

}
